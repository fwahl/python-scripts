#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Python helper script for krop PDF cropping tool."""

__author__ = "Florian Wahl"
__copyright__ = "Copyright 2018, Florian Wahl"
__version__ = "0.1"
__maintainer__ = "Florian Wahl"
__email__ = "florian.wahl@gmail.com"
__status__ = "Development"

import sys
import os

if len(sys.argv) not in [2, 3]:
    print("Florians krop helper script.")
    print("Usage:")
    print("krop.py infile autokrops infile and save result to ../infile")
    print("krop.py infile outfile autocrops infile and save result to outfile")
    exit()

elif len(sys.argv) == 2:
    infile = sys.argv[1]
    outfile = "../" + sys.argv[1]
    
elif len(sys.argv) == 3:
    infile = sys.argv[1]
    outfile = "../" + sys.argv[1]

os.system("krop --autotrim --go " + infile + " -o " + outfile)
